import React, {Component} from 'react';
import {Nav, NavItem, NavLink, TabContent, TabPane} from 'reactstrap';
import classnames from 'classnames';

import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Row, Col} from 'reactstrap';

import {salesOrderService} from '../../services';
import {userActions, alertActions} from '../../actions';
import { get } from 'http';

const tableQuery = [{entityTable: 'Orders', objName: 'soNumber'}, 
                    {entityTable: 'Stock', objName: 'saleOrderDetailAutoKey'}, 
                    {entityTable: 'Shipment', objName: 'saleOrderDetailAutoKey'}];
const txnQuery = [{code: 'H', display: 'Lease Adjustment'},
                  {code: 'I', display: 'Brokered Repair'},
                  {code: 'O', display: 'Overhaul'},
                  {code: 'R', display: 'Repair'},
                  {code: 'L', display: 'Labor'},
                  {code: 'T', display: 'Rental Lease Charge'},
                  {code: 'M', display: 'Credit Memo'},
                  {code: 'X', display: 'Misc'},
                  {code: 'J', display: 'Invoice Adjustment'},
                  {code: 'S', display: 'Part Sale'},
                  {code: 'E', display: 'Exchange'},
                  {code: 'A', display: 'Alternate Pricing'},
                  {code: 'F', display: 'Freight'},
                  {code: 'N', display: 'Non-Stock'},
                  {code: 'B', display: 'Rental Lease Part'},
                  {code: 'K', display: 'Kit'},
                  {code: 'C', display: 'Credit'}];
const sysLocalFormat = window.navigator.userLanguage || window.navigator.language;
const dateFormat = {year: 'numeric', month: '2-digit', day: '2-digit'};

class DataGridRowDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: '1',
      loadingOrderData: true,
      loadingShipmentData: true,
      shipmentDetailsData: [],
    };
  }

  toggleTab(tab) {
    if(this.state.activeTab !== tab) {
      this.setState({activeTab: tab});
    }
  }

  onRowTxnTypeTemplate(rowObj) {
    let txnCodeDisplay = txnQuery.filter(txn => txn.code == rowObj.routeCode);
    return (
      <div> { 
        txnCodeDisplay[0].display 
       } 
      </div>
    );
  }

  onRowShipDateFormatTemplate(rowObj) {
    let date = new Date(rowObj.shipDate);
    return (
      <div> { rowObj.shipDate && new Intl.DateTimeFormat(sysLocalFormat, dateFormat).format(date) } </div>
    );
  }

  onRowNxtShipDateFormatTemplate(rowObj) {
    let date = new Date(rowObj.nextShipDate);
    return (
      <div> { rowObj.nextShipDate && new Intl.DateTimeFormat(sysLocalFormat, dateFormat).format(date) } </div>
    );
  }

  onRowPriceFormatTemplate(rowObj) {
    const sysLocalFormat = window.navigator.userLanguage || window.navigator.language;
    let options = {minimumFractionDigits: 2, maximumFractionDigits: 2};
    return (
      <div style={{textAlign: 'right'}}> {
        new Intl.NumberFormat(sysLocalFormat, options).format(rowObj.customerPrice)
       } 
      </div>
    );
  }

  getOrderDetailsList() {
    const rowData = this.props.datagridRowData;
    let strQryA = rowData.soNumber;
    let objNameQryA = tableQuery[0].objName;
    let pageRows = rowData.numberOfItems;
    let skip = 0; // set 0 to start on beginning of index

    // Get SOnumber from previous orderDetailsData
    let orderSoNumber = this.state.orderDetailsData ? this.state.orderDetailsData[0].soNumber : null;

    if(rowData.numberOfItems > 1 && orderSoNumber != rowData.soNumber) {
      salesOrderService.searchOrdersList(strQryA, objNameQryA, pageRows, skip).then(result => {
        // Sort data result by item number
        let orderData = result.data.sort((a, b) => (a.itemNumber > b.itemNumber) ? 1 : -1);
        // Set response results to orderDetailsData 
        this.setState({orderDetailsData: orderData, loadingOrderData: false, loadingShipmentData: true});

        // Get shipment and stock data
        this.getShipmentStockData(orderData).then(() => {
          this.setState({loadingShipmentData: false});
        }, error => {
          this.httpErrorHandler(error);
        });

      }, error => {
        this.httpErrorHandler(error);
      });

    } else if(orderSoNumber == null || orderSoNumber != rowData.soNumber) {
      let orderData = [{...rowData}];
      // Set response results to orderDetailsData 
      this.setState({orderDetailsData: orderData, loadingOrderData: false, loadingShipmentData: true});
      
      // Get shipment and stock data
      this.getShipmentStockData(orderData).then(() => {
        this.setState({loadingShipmentData: false}); 
      }, error => {
        this.httpErrorHandler(error);
      });
    }

  }

  getShipmentStockData(orderData) {
    var qtyInvoiced = 0;
    this.datasource = []; // Initialised data storages
    this.setState({shipmentDetailsData: []}); //console.log(`->Orders:`, orderData)//FOR DEBUGGING

    let orderMap = new Promise((resolve, reject) => {
      return orderData.map(order => {
        var soAutoKey = order.SaleOrderDetailAutoKey;                       // Get sales order details auto key
        qtyInvoiced = order.qtyInvoiced > 0 ? qtyInvoiced+1 : qtyInvoiced;  // Accumulate number of invoices

        const getShipmentData = this.getShipmentDetails(soAutoKey);
        const getStockData = this.getStockDetails(soAutoKey);

        getShipmentData.then(shipData => {   //console.log(`ShipData: ${soAutoKey} ${qtyInvoiced}`, shipData.data);//FOR DEBUGGING
          shipData.data.map(shipObj => {     //console.log(`shipMap: ${soAutoKey} Itm:${shipObj.itemNumber} ${qtyInvoiced} Inv:${shipObj.invocieNumber}`);//FOR DEBUGGING
            var shipObjData = shipObj;
            getStockData.then(stockData => { //console.log(`StockData: ${soAutoKey}`, stockData.data);//FOR DEBUGGING
              stockData.data.map(stockObj => {
                let shipmentObjData = {...shipObjData, ...stockObj};
                this.datasource.push(shipmentObjData);
                // Sort datasource by item number
                this.datasource.sort((a, b) => (a.itemNumber > b.itemNumber) ? 1 : -1);
                this.datasource.length >= qtyInvoiced ? resolve() : null;
              });
              qtyInvoiced = stockData.data.length == 0 ? qtyInvoiced-1 : qtyInvoiced; //Decrease qtyInvoiced
              this.datasource.length >= qtyInvoiced ? resolve() : null;
            }, err => {reject(err)});
          });
          qtyInvoiced = shipData.data.length == 0 ? qtyInvoiced-1 : qtyInvoiced; //Decrease qtyInvoiced
          this.datasource.length >= qtyInvoiced ? resolve() : null;
        }, err => {reject(err)});

      });
    });

    return new Promise((resolve,reject) => {
      orderMap.then(() => {
        setTimeout(() => {
          this.setState({shipmentDetailsData: this.datasource}, () => {
            return resolve();
          });
        }, 2000); 
      }, err => {
        return reject(err);
      });
    });
  }

  getShipmentDetails(soAutoKey) {
    let strQryA = soAutoKey;
    let objNameQryA = tableQuery[2].objName;
    let pageRows = this.props.datagridRowData.numberOfItems;
    let skip = 0; // set 0 to start on beginning of index
    return new Promise((resolve, reject) => {
      return salesOrderService.searchShipment(strQryA, objNameQryA, pageRows, skip).then(data => {
          return resolve(data);
        }, err => { 
          return reject(err)
        });
    });
  }

  getStockDetails(soAutoKey) {
    let strQryA = soAutoKey;
    let objNameQryA = tableQuery[1].objName;
    let pageRows = this.props.datagridRowData.numberOfItems;
    let skip = 0; // set 0 to start on beginning of index
    return new Promise((resolve, reject) => {
      return salesOrderService.searchStock(strQryA, objNameQryA, pageRows, skip).then(data => {
          return resolve(data);
        }, err => { 
          return reject(err)
        });
    });
  }

  httpErrorHandler(error) {
    let httpStatus = error.response != undefined ? error.response.status : 404;
    if(httpStatus >= 500 || httpStatus == 404) {  // Added to handle error(s)
      // this.props.dispatch(alertActions.error('Network Error.') );
      this.setState({loadingOrderData: false, loadingShipmentData: false});
    } else if(httpStatus == 401) {
      userActions.msalLogout()
    } else {
      // this.props.dispatch(failure(error.toString()) );
      // this.props.dispatch(alertActions.error(error.toString()) );
      this.setState({loadingOrderData: false, loadingShipmentData: false});
    }
  }

  orderDetailsTemplate() {
    const tableColumn = [
      {field: 'routeCode', header: 'Txn Type', body: (this.onRowTxnTypeTemplate.bind(this)), style: {width: '9%', fontSize: '11px'}},
      {field: 'pnUpper', header: 'Part No', style: {width: '11%', fontSize: '10px'}},
      {field: 'itemNumber', header: 'Item No', style: {width: '6%', fontSize: '11px', textAlign: 'center'}},
      {field: 'qtyOrdered', header: 'Qty Ordered', style: {width: '8%', fontSize: '11px', textAlign: 'center'}},
      {field: 'qtyInvoiced', header: 'Qty Shipped', style: {width: '8%', fontSize: '11px', textAlign: 'center'}},
      {field: 'qtyReserved', header: 'Await Shipping', style: {width: '8%', fontSize: '11px', textAlign: 'center'}},
      {field: 'nextShipDate', header: 'Next Ship Date', body: (this.onRowNxtShipDateFormatTemplate.bind(this)), style: {width: '10%', fontSize: '11px', textAlign: 'center'}},
      {field: 'descriptionUpper', header: 'Part Desc', style: {width: '15%', fontSize: '10px'}},
      {field: 'customerPrice', header: 'Price', body: (this.onRowPriceFormatTemplate.bind(this)), style: {width: '10%', fontSize: '11px'}},
      {field: 'currencyCode', header: 'Currency', style: {width: '7%', fontSize: '11px', textAlign: 'center'}},
      {field: null, header: 'Invoice Docs', style: {width: '8%', fontSize: '11px'}},
    ];
    const dynamicColumns = tableColumn.map((col, i) => {
      return <Column key={col.field} field={col.field} header={col.header} headerStyle={col.style}
                     body={col.body} bodyStyle={col.style} />
    });

    this.getOrderDetailsList();

    return (
      <div className="row">
        <div className="col-12">
          <DataTable value={this.state.orderDetailsData} lazy={true} loading={this.state.loadingOrderData} 
                     resizableColumns={true} autoLayout={true} >
            {dynamicColumns}
          </DataTable>
        </div>
      </div>
    );
  }

  shippingDetailsTemplate() {
    const tableColumn = [
      {field: 'itemNumber', header: 'Item No', style: {width: '7%', fontSize: '11px', textAlign: 'center'}},
      {field: 'airwayBill', header: 'Airway Bill', style: {width: '13%', fontSize: '11px', textAlign: 'center'}},
      {field: null, header: 'Airway Bill Docs', style: {width: '8%', fontSize: '11px'}},
      {field: null, header: 'Track Airway Bill', style: {width: '12%', fontSize: '11px'}},
      {field: 'shipDate', header: 'Shipment Date', body: (this.onRowShipDateFormatTemplate.bind(this)), style: {width: '11%', fontSize: '11px', textAlign: 'center'}},
      {field: null, header: 'POD Image', style: {width: '8%', fontSize: '11px'}},
      {field: null, header: 'Customs Invoice', style: {width: '10%', fontSize: '11px'}},
      {field: 'invocieNumber', header: 'Invoice No', style: {width: '11%', fontSize: '11px', textAlign: 'center'}},
      {field: 'serialNumber', header: 'Serial No', style: {width: '11%', fontSize: '11px', textAlign: 'center'}},
      {field: null, header: 'Images', style: {width: '9%', fontSize: '11px'}},
    ];
    const dynamicColumns = tableColumn.map((col, i) => {
      return <Column key={col.field} field={col.field} header={col.header} headerStyle={col.style}
                     body={col.body} bodyStyle={col.style} />
    });

    return (
      <div className="row">
        <div className="col-12">
          <DataTable value={this.state.shipmentDetailsData} lazy={true} loading={this.state.loadingShipmentData}
                     resizableColumns={true} autoLayout={true} >
            {dynamicColumns}
          </DataTable>
        </div>
      </div>
    );
  }

  dataGridRowTemplate() {
    let tabValue = this.props.tabValue;
    let rowObj = this.props.datagridRowData;
    
    return  (
      <div className="p-grid p-fluid" style={{padding: '0.25em'}}>
        <div className="animated fadeIn">
        { rowObj &&
            <div>
              { tabValue == 'Orders' && <h4 style={{fontWeight:'bold'}}>{`Sales Order No:`} {rowObj.soNumber}</h4>}
              { tabValue == 'Exchanges' && <h4 style={{fontWeight:'bold'}}>{`Customer PO No:`} {rowObj.companyRefNumber}</h4>}
              { tabValue == 'Invoices' && <h4 style={{fontWeight:'bold'}}>{`${tabValue.substring(0,tabValue.length -1)} No:`} {rowObj.invocieNumber}</h4>}
                <div>
                  <Row>
                    <Col xs="12" sm="4" lg="4">
                      {tabValue == 'Orders' && <div> <div>Customer PO No.</div> <small className="text-muted">{rowObj.companyRefNumber}</small> </div>}
                      {tabValue == 'Exchanges' && <div> <div>Sales Order No.</div> <small className="text-muted">{rowObj.soNumber}</small> </div>}
                      {tabValue == 'Invoices' && <div> <div>Customer PO No.</div> <small className="text-muted">{rowObj.companyRefNumber}</small> </div>}
                    </Col>
                    <Col xs="12" sm="4" lg="4">
                      <div>For-The-Attention-Of(FAO)</div>
                      <small className="text-muted">{rowObj.attention}</small>
                    </Col>
                    <Col xs="12" sm="4" lg="4">
                      <div>Status</div>
                      { rowObj.openFlag == 'F' && <small className="text-muted" style={{background: `linear-gradient(to right, #ffcc99 0%, #ffcc99 100%)`, padding: "0.5px 0.5px", textAlign: "center",}}>CLOSED</small>}
                      { rowObj.openFlag == 'T' && <small className="text-muted" style={{background: `linear-gradient(to right, #00ff00 0%, #00ff00 100%)`, padding: "0.5px 0.5px", textAlign: "center",}}>OPEN</small>}
                    </Col>
                  </Row>
                  <Row style={{paddingTop: '5px'}}>
                    <Col xs="12" sm="12" lg="12">
                      <div>Ship To</div> <small className="text-muted">{`${rowObj.companyName}, ${rowObj.shipAddress1}, ${rowObj.shipAddress2}`}</small>
                    </Col>
                  </Row>
                </div> 
              <br />
            </div>
          }
          
          <hr />
          <Nav className="nav nav-pills">
            <NavItem className="px-3">
              <NavLink className={classnames({ active: this.state.activeTab === '1' })}
                      onClick={() => { this.toggleTab('1'); }} style={{cursor: 'pointer'}}>
                  <i className="fa fa-list-alt" />&nbsp;Order Details
              </NavLink>
            </NavItem>
            <NavItem className="px-3">
              <NavLink className={classnames({ active: this.state.activeTab === '2' }) }
                      onClick={() => { this.toggleTab('2'); }} style={{cursor: 'pointer'}}>
                  {this.state.loadingShipmentData && <i className="fa fa-spinner fa-pulse fa-fw" />}&nbsp;&nbsp;
                  <i className="fa fa-plane" />&nbsp;Shipping Details&nbsp;&nbsp;
                  {!this.state.loadingShipmentData &&
                    <span className="badge badge-pill badge-primary">{this.state.shipmentDetailsData.length}</span>
                  }
              </NavLink>
            </NavItem>
          </Nav>

          <TabContent activeTab={this.state.activeTab} style={{borderStyle: 'none'}}>
            <TabPane tabId="1" style={{padding: '0em'}}>
              <div>{this.orderDetailsTemplate()}</div>
            </TabPane>
            <TabPane tabId="2" style={{padding: '0em'}}>
              <div>{this.shippingDetailsTemplate()}</div>
            </TabPane>
          </TabContent>
          <hr />  
        </div>  
      </div>
    );   
  }

  render() {
    return (
      <div>
        { this.props.datagridRowData && this.dataGridRowTemplate() }
      </div>
    );
  }
}

export default DataGridRowDetails;

// src/reducers/index.js

import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { challenged } from './challenged.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { salesorder } from './salesorder.reducer';

const rootReducer = combineReducers({
  authentication,
  registration,
  challenged,
  users,
  alert,
  salesorder,
});

export default rootReducer;
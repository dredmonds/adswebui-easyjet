// src/components/HeaderNavMenus.js

import React, {Component} from 'react';
import {connect} from 'react-redux';
import { 
  Nav, NavItem, NavLink,
  Popover, ListGroup, ListGroupItem
} from 'reactstrap';

class HeaderNavMenus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      navOrdersOpen: false,
      navShipmentsOpen: false,
      navInventoryOpen: false
    };

    // Bind methods
    this.navMenus = this.navMenus.bind(this);
    this.navOrdersToggle = this.navOrdersToggle.bind(this);
    this.navShipmentsToggle = this.navShipmentsToggle.bind(this);
    this.navInventoryToggle = this.navInventoryToggle.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
  }

  // Methods of events
  navOrdersToggle() {
    this.setState({navOrdersOpen: !this.state.navOrdersOpen})
  }
  navShipmentsToggle() {
    this.setState({navShipmentsOpen: !this.state.navShipmentsOpen})
  }
  navInventoryToggle() {
    this.setState({navInventoryOpen: !this.state.navInventoryOpen})
  }
  onMouseLeave() {
    this.setState({
      navOrdersOpen: false,
      navShipmentsOpen: false,
      navInventoryOpen: false
    });
  }

  navMenus(user) {
    return (
      <Nav className="d-md-down-none" nav-bar="true">
        {/* <NavItem className="px-3">
          <NavLink href="#/dashboard"><i className="icon-speedometer"/>&nbsp;Dashboard</NavLink>
        </NavItem> */}
        {/* <NavItem className="px-3">
          <NavLink id="navOrders" href="#" onClick={this.navOrdersToggle}><i className="fa fa-list-alt"/>&nbsp;ORDERS&nbsp;<i className="fa fa-angle-down" /></NavLink>
          <Popover placement="bottom" isOpen={this.state.navOrdersOpen} target="navOrders" toggle={this.navOrdersToggle}>
            <ListGroup flush>
              { user.userPermissions.some(str => str.permission.name == 'Order.Read')  && 
                <ListGroupItem tag="a" href="#/orders/sales"><i className="icon-star"/>&nbsp;Sales (Customer)</ListGroupItem>
              }
              { user.userPermissions.some(str => str.permission.name == 'ExchangeOrderRequest.Read') &&
                <ListGroupItem tag="a" href="#/orders/pbh"><i className="icon-star"/>&nbsp;PBH (Contracts)</ListGroupItem>
              }
              { user.repairOrderPrefix &&
                <ListGroupItem tag="a" href="#/orders/adhoc"><i className="icon-star"/>&nbsp;AdHoc (Repairs)</ListGroupItem>
              }
              { user.userPermissions.some(str => str.permission.name == 'ConsumableOrderRequest.Read') &&
                <ListGroupItem tag="a" href="#/orders/rfq"><i className="icon-star"/>&nbsp;RFQ (Consumables)</ListGroupItem>
              }
            </ListGroup>
          </Popover>
        </NavItem> */}
        {/* <NavItem className="px-3">
          <NavLink id="navShipments" href="#" onClick={this.navShipmentsToggle}><i className="fa fa-plane"/>&nbsp;SHIPMENTS&nbsp;<i className="fa fa-angle-down" /></NavLink>
          <Popover placement="bottom" isOpen={this.state.navShipmentsOpen} target="navShipments" toggle={this.navShipmentsToggle}>
            <ListGroup flush>
              <ListGroupItem tag="a" href="#/shipments/shipments"><i className="icon-star"/>&nbsp;Shipments</ListGroupItem>
            </ListGroup>
          </Popover>
        </NavItem> */}
        {/* <NavItem className="px-3">
          <NavLink id="navInventory" href="#"  onClick={this.navInventoryToggle}><i className="fa fa-cubes" />&nbsp;INVENTORY&nbsp;<i className="fa fa-angle-down" /></NavLink>
          <Popover placement="bottom" isOpen={this.state.navInventoryOpen} target="navInventory" toggle={this.navInventoryToggle}>
            <ListGroup flush>
              <ListGroupItem tag="a" href="#/inventory/mystock"><i className="icon-star"/>&nbsp;My Stock</ListGroupItem>
            </ListGroup>
          </Popover>
        </NavItem> */}
      </Nav>
    );
  }

  render() {
    const { user } = this.props;
    return (
      this.navMenus(user)
    );
  }
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user
  };
}

const connectedHeaderNavMenu = connect(mapStateToProps)(HeaderNavMenus);
export default connectedHeaderNavMenu;
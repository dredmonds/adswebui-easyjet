const routes = {
  '/login': 'Login',
  '/register': 'Register',
  '/': 'Home',
  '/dashboard': 'Dashboard',
  '/orders': 'Orders',
  '/orders/sales': 'Sales',
};
export default routes;

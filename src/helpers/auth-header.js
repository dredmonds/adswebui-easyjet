// src/helpers/auth-headers.js

import config from 'config';

export function authHeader() {
  // return authorization header with jwt token
  let auth = JSON.parse(sessionStorage.getItem(`ads.auth.token.key${config.adsMsalConfig.clientID}`));

  if(auth && auth.accessToken) {
    return { 'Authorization': 'Bearer ' + auth};
  } else {
    return {};
  }
}
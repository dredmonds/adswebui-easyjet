import * as Msal from 'msal';
import config from 'config';
import {ApiClient} from './client';

const {API, adsMsalConfig} = config;
const msalcfg = { API, adsMsalConfig, options: {
                              validateAuthority: false,  // set to false for v0.1.1+
                              storeAuthStateInCookie: true, //fix for the authentication loop issues on IE and Edge
                  } };
const adsMsalApplication = new Msal.UserAgentApplication(msalcfg.adsMsalConfig.clientID, msalcfg.adsMsalConfig.authority, authCallback, msalcfg.options);

const client = new ApiClient();

export const adsMsalAuth = {
  login,
  logout,
  getTokenSilent,
  getTokenPoup,
  getUserInfo,
  callApiAccessToken,
};

function loggerCallback(logLevel, message, piiEnabled) {
  console.log(message);
}

function authCallback(errorDesc, token, error, tokenType) {
  if(tokenType == 'id_token') {
    return {
      user: getUserName(),
      accessToken: token,
      authTest: callApiAccessToken(token)
    };
  }else {
    return {error, errorDesc};
  }
}

function login() {
  return new Promise(function(resolve, reject) {
    adsMsalApplication.loginPopup(msalcfg.adsMsalConfig.b2cScopes).then(res => {
      getTokenSilent(msalcfg.adsMsalConfig.b2cScopes).then(accessToken => {
        callApiAccessToken(accessToken).then(res => {
            let {idToken} = getUserInfo();
            return resolve({...res, accessToken, 'tokenExp': {'exp': idToken.exp} });
          }, err => {
            return reject (err);
          });
      }, error => {
        getTokenPoup(msalcfg.adsMsalConfig.b2cScopes).then(accessToken => {
          callApiAccessToken(accessToken).then(res => {
            let {idToken} = getUserInfo();
            return resolve({...res, accessToken, 'tokenExp': {'exp': idToken.exp} });
          }, err => {
            return reject (err);
          });
        }, err => {
          return reject (err);
        });
      });
    }, function(error) {
      return reject (error);
    });

  })
}

function logout() {
  return adsMsalApplication.logout();
}

function getTokenSilent(b2cScopes) {
  return new Promise(function(resolve, reject) {
    adsMsalApplication.acquireTokenSilent(b2cScopes).then(response => {
      return resolve (response);
    }, error => {
      return reject (error);
    });
  }, function(reject) {
    return reject (reject);
  })
}

function getTokenPoup(b2cScopes) {
  return new Promise(function(resolve, reject) {
    adsMsalApplication.acquireTokenPopup(b2cScopes).then(response => {
      return resolve (response);
    }, error => {
      return resolve (error);
    });
  }, function(reject) {
    return reject (reject);
  })
}

function getUserInfo() {
  return adsMsalApplication.getUser();
}

function callApiAccessToken(accessToken) {
  return new Promise(function(resolve, reject) {
    client.get(`${msalcfg.API}/api/Identity`, {headers: {'Authorization': 'Bearer ' + accessToken}} )
    .then(response => {
      return resolve (response.data.user);
    }, error => {
      return reject (error);
    });
  }, function(reject) {
    return reject (reject);
  })
}